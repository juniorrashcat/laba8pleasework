#include "pch.h"
#include "CppUnitTest.h"
#include "../main-project/konf_description.h"
#include "../main-project/process.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace unittestproject
{
	//        
	//             ,
	//   ,     
	konf_description* build_subscription(int start_hours, int start_minutes, int finish_hours, int finish_minutes)
	{
		konf_description* subscription = new konf_description;
		subscription->start.hours = start_hours;
		subscription->start.minutes = start_minutes;
		subscription->finish.hours = finish_hours;
		subscription->finish.minutes = finish_minutes;
		return subscription;
	}

	//       
	void delete_subscription(konf_description* array[], int size)
	{
		for (int i = 0; i < size; i++)
		{
			delete array[i];
		}
	}

	TEST_CLASS(unittestproject)
	{
	public:
		TEST_METHOD(TestMethod1) //     
		{
			konf_description* subscriptions[3];
			subscriptions[0] = build_subscription(1, 0, 2, 0); // 60
			subscriptions[1] = build_subscription(2, 1, 2, 8); // 7 
			subscriptions[2] = build_subscription(3, 0, 3, 0); // 0
			Assert::AreEqual(60, process(subscriptions, 3));
			delete_subscription(subscriptions, 3);
		}

		TEST_METHOD(TestMethod2) //        
		{
			konf_description* subscriptions[3];
			subscriptions[0] = build_subscription(20, 0, 20, 15); // 15 
			subscriptions[1] = build_subscription(22, 1, 22, 20); // 19 
			subscriptions[2] = build_subscription(19, 1, 19, 15); // 14 
			Assert::AreEqual(19, process(subscriptions, 3));
			delete_subscription(subscriptions, 3);
		}

	};
}
