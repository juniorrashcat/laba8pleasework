#include <iostream>
#include <iomanip>

using namespace std;

#include "konf_description.h"
#include "file_reader.h"
#include "constants.h"
#include "process.h"
#include "filter.h"

void output(konf_description* konf_description)
{
    /********** вывод конференции **********/
    cout << "Лектор...........: ";
    // вывод фамилии автора
    cout << konf_description->author.last_name << " ";
    // вывод первой буквы имени автора
    cout << konf_description->author.first_name[0] << ". ";
    // вывод первой буквы отчества автора
    cout << konf_description->author.middle_name[0] << ".";
    cout << ", ";
    // вывод названия
    cout << '"' << konf_description->title << '"';
    cout << '\n';
    /********** вывод времени **********/
    // вывод часов
    cout << "Время начала.....: ";
    cout << konf_description->start.hours << '-';
    // вывод минут
    cout << konf_description->start.minutes << '-';
    cout << '\n';
    cout << "Время конца.....: ";
    cout << konf_description->finish.hours << '-';
    // вывод минут
    cout << konf_description->finish.minutes << '-';
    cout << '\n';
    cout << '\n';
}


int main()
{
	setlocale(LC_ALL, "Russian");
    system("chcp 1251");
    cout << "Лабораторная работа №8. GIT\n";
    cout << "Вариант №2. Библиотечный абонемент\n";
    cout << "Автор: Шенделяр Артём\n\n";
    cout << "Group: 16\n";
    konf_description* subscriptions[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", subscriptions, size);
        cout << "***** Конференции *****\n\n";
        
		for (int i = 0; i < size; i++)
		{
			output(subscriptions[i]);
		}
		bool (*check_function)(konf_description*) = NULL;
		cout << "\n     :\n";
		cout << "1) Ivanova Ivana Ivanovna      \n";
		cout << "2) bolshie 15 minutes \n";
		cout << "3) function for tests  ,      \n";
		cout << "\n   : ";
		int item;
		cin >> item;
		cout << '\n';
		switch (item)
		{
		case 1:
			check_function = check_konf_description_by_author; //       
			cout << "***** Сортировка по имени *****\n\n";
			break;
		case 2:
			check_function = check_konf_description_by_date; //       
			cout << "*****Сортировка по типу кто не хочет умереть 2 часа на конференции *****\n\n";
			break;
		case 3:
		{
			int hours = process(subscriptions, size);
			cout << "*****   ,       *****\n\n";
			cout << hours/60 << ":" << hours%60 << "0" << "\n";
			break;
		}

		default:
			throw "  ";
		}
		if (check_function)
		{
			int new_size;
			konf_description** filtered = filter(subscriptions, size, check_function, new_size);
			for (int i = 0; i < new_size; i++)
			{
				output(filtered[i]);
			}
			delete[] filtered;
		}
		for (int i = 0; i < size; i++)
		{
			delete subscriptions[i];
		}
	}
	catch (const char* error)
	{
		cout << error << '\n';
	}
	return 0;
}
