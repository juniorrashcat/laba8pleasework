#pragma once
#ifndef CONF_DESCRIPTION_H
#define CONF_DESCRIPTION_H

#include "constants.h"

struct mytime
{
    int hours;
    int minutes;
};

struct person
{
    char first_name[MAX_STRING_SIZE];
    char middle_name[MAX_STRING_SIZE];
    char last_name[MAX_STRING_SIZE];
};

struct konf_description
{
    mytime start;
    mytime finish;
    person author;
    char title[MAX_STRING_SIZE];
};

#endif