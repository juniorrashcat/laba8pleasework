#pragma once
#ifndef FILTER_H
#define FILTER_H

#include "konf_description.h"

konf_description** filter(konf_description* array[], int size, bool (*check)(konf_description* element), int& result_size);
bool check_konf_description_by_author(konf_description* element);
bool check_konf_description_by_date(konf_description* element);
#endif
