#include "process.h"
int process(konf_description* array[], int size) {
	int date = 0;
	int mytime = 0;
	for (int i = 0; i < size; i++) {
		mytime = (array[i]->finish.hours - array[i]->start.hours) * 60 + (array[i]->finish.minutes - array[i]->start.minutes);
		if (date < mytime) {
			date = mytime;
		}
	}
	return date;
}