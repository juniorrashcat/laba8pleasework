#include "filter.h"
#include <cstring>
#include <iostream>

konf_description** filter(konf_description* array[], int size, bool (*check)(konf_description* element), int& result_size)
{
	konf_description** result = new konf_description * [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool check_konf_description_by_author(konf_description* element)
{
	return strcmp(element->author.first_name, "�����") == 0 &&
		strcmp(element->author.middle_name, "���������") == 0 &&
		strcmp(element->author.last_name, "�������") == 0;
}

bool check_konf_description_by_date(konf_description* element)
{
	return ((element->finish.hours - element->start.hours)*60 + (element->finish.minutes - element->start.minutes)) > 15;
}
