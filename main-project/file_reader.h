#pragma once
#ifndef FILE_READER_H
#define FILE_READER_H

#include "konf_description.h"

void read(const char* file_name, konf_description* array[], int& size);

#endif